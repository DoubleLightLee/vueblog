package com.ldd.mapper;

import com.ldd.entity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ldd
 * @since 2021-08-16
 */
public interface MessageMapper extends BaseMapper<Message> {

}
