package com.ldd.mapper;

import com.ldd.entity.Blog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ldd
 * @since 2021-06-06
 */
@Mapper
public interface BlogMapper extends BaseMapper<Blog> {

}
