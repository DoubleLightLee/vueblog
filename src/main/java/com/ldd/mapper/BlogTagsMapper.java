package com.ldd.mapper;

import com.ldd.entity.BlogTags;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ldd
 * @since 2021-07-30
 */
public interface BlogTagsMapper extends BaseMapper<BlogTags> {

}
