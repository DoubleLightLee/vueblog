package com.ldd.mapper;

import com.ldd.entity.Type;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ldd
 * @since 2021-06-14
 */
@Mapper
public interface TypeMapper extends BaseMapper<Type> {

}
