package com.ldd.mapper;

import com.ldd.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ldd
 * @since 2021-06-06
 */
public interface UserMapper extends BaseMapper<User> {

}
