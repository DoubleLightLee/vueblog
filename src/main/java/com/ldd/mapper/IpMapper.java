package com.ldd.mapper;

import com.ldd.entity.Ip;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ldd
 * @since 2021-08-13
 */
public interface IpMapper extends BaseMapper<Ip> {

}
