package com.ldd.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ldd.common.lang.Result;
import com.ldd.entity.Tag;
import com.ldd.mapper.TagMapper;
import com.ldd.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ldd
 * @since 2021-06-14
 */
@RestController
public class TagController {

    @Autowired
    TagService tagService;

    @Autowired
    TagMapper tagMapper;

    @GetMapping("/getTags")
    public Result getTags(){
        List<Tag> tagList = new ArrayList<>();
        QueryWrapper<Tag> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id","name").orderByAsc("id").last("limit 5");
        tagList = tagMapper.selectList(queryWrapper);
//        System.out.println(tagList);
        return Result.succ(tagList);
    }

}
