package com.ldd.controller;


import com.ldd.common.lang.Result;
import com.ldd.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ldd
 * @since 2021-08-13
 */
@RestController
public class IpController {

    @Autowired
    private RedisUtil redisUtil;

    @PostMapping("/getIPCount")
    public Result getIPCount(@RequestBody String userIP){
        if(userIP != null){
            redisUtil.sSet("userIP", userIP);
            int redisIPCount = new Long(redisUtil.sGetSetSize("userIP")).intValue();
            if(null == redisUtil.get("nowIPCount")){
                redisUtil.set("nowIPCount", 0);
            }
            Object o = redisUtil.get("nowIPCount");
            int updateIPCount = redisIPCount + Integer.parseInt(String.valueOf(o));
//            System.out.println(updateIPCount);
            return Result.succ(updateIPCount);
        } else {
            return Result.fail("未获取到ip地址");
        }
    }

}
