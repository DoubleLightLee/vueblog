package com.ldd.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ldd.common.lang.Result;
import com.ldd.entity.Type;
import com.ldd.mapper.TypeMapper;
import com.ldd.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ldd
 * @since 2021-06-14
 */
@RestController
public class TypeController {

    @Autowired
    TypeService typeService;

    @Autowired
    TypeMapper typeMapper;

    @GetMapping("/getTypes")
    public Result getTypes(){
        List<Type> typeList = new ArrayList<>();
        QueryWrapper<Type> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id","name").orderByAsc("id").last("limit 5");
        typeList = typeMapper.selectList(queryWrapper);
//        System.out.println(typeList);
        return Result.succ(typeList);
    }

}
