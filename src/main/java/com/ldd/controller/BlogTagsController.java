package com.ldd.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ldd
 * @since 2021-07-30
 */
@RestController
@RequestMapping("/blog-tags")
public class BlogTagsController {

}
