package com.ldd.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ldd.common.lang.Result;
import com.ldd.entity.Message;
import com.ldd.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ldd
 * @since 2021-08-16
 */
@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @PostMapping("/sendMsg")
    public Result sendMsg(@Validated @RequestBody Message message){
//        System.out.println(message.toString());
        Message temp = new Message();
        temp.setCreated(LocalDateTime.now());
        temp.setStatus(0);
        BeanUtil.copyProperties(message, temp, "id", "created", "status");
        messageService.save(temp);
        return Result.succ("操作成功");
    }

    public Result getMsgList(Integer currentPage){
        if (currentPage == null || currentPage < 1){
            currentPage = 1;
        }
        Page page = new Page(currentPage, 5);
        IPage pageData = messageService.page(page, new QueryWrapper<Message>().eq("status", 1).orderByDesc("created"));
        return Result.succ(pageData);
    }

}
