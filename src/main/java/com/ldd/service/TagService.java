package com.ldd.service;

import com.ldd.entity.Tag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ldd
 * @since 2021-06-14
 */
public interface TagService extends IService<Tag> {

}
