package com.ldd.service;

import com.ldd.entity.Message;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ldd
 * @since 2021-08-16
 */
public interface MessageService extends IService<Message> {

}
