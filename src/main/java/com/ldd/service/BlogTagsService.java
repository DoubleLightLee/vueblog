package com.ldd.service;

import com.ldd.entity.BlogTags;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ldd
 * @since 2021-07-30
 */
public interface BlogTagsService extends IService<BlogTags> {

}
