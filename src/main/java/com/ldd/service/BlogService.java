package com.ldd.service;

import com.ldd.entity.Blog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ldd
 * @since 2021-06-06
 */
public interface BlogService extends IService<Blog> {

}
