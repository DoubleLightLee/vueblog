package com.ldd.service;

import com.ldd.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ldd
 * @since 2021-06-06
 */
public interface UserService extends IService<User> {

}
