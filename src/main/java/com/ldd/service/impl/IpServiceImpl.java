package com.ldd.service.impl;

import com.ldd.entity.Ip;
import com.ldd.mapper.IpMapper;
import com.ldd.service.IpService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ldd
 * @since 2021-08-13
 */
@Service
public class IpServiceImpl extends ServiceImpl<IpMapper, Ip> implements IpService {

}
