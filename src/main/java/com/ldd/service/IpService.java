package com.ldd.service;

import com.ldd.entity.Ip;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ldd
 * @since 2021-08-13
 */
public interface IpService extends IService<Ip> {

}
