package com.ldd.config;

import com.ldd.entity.Ip;
import com.ldd.service.IpService;
import com.ldd.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;

@Configuration  //主要用于标记配置类，兼备Component效果
@EnableScheduling   //开启定时任务
public class ScheduledTask {

    @Autowired
    private IpService ipService;

    @Autowired
    private RedisUtil redisUtil;

//    @Scheduled(cron = "0/30 * * * * ?") //每隔30s更新一次
    //每天凌晨3点更新数据库
    @Scheduled(cron = "0 0 3 * * ?")
    private void ConfigureTasks(){
        System.out.println("----------定时任务开始执行----------");
        //获取redis缓存中的用户IP地址userIP的总数
        int redisIPCount = new Long(redisUtil.sGetSetSize("userIP")).intValue();
        System.out.println(LocalDateTime.now() + "当前redis缓存中的IP数量为：" + redisIPCount);
        //查询数据库中的用户IP地址总数
        Ip lastIPCount = ipService.getById("1");
        System.out.println(LocalDateTime.now() + "数据库中IP数量为：" + lastIPCount.getIpCount());
        //将redis缓存中的用户IP地址总数加到数据库中，并更新数据库
        Ip updateIPCount = new Ip();
        updateIPCount.setId(1);
        updateIPCount.setIpCount(lastIPCount.getIpCount() + redisIPCount);
        ipService.updateById(updateIPCount);
        //清空redis中的缓存
        redisUtil.del("userIP");
        //获取更新后数据库中的用户IP地址总数，并将当前用户IP地址总数存放到缓存中
        Ip nowIPCount = ipService.getById(1);
        redisUtil.set("nowIPCount", nowIPCount.getIpCount());
        System.out.println(LocalDateTime.now() + "更新后现在数据库中的IP数量：" + nowIPCount.getIpCount());
        System.out.println("----------定时任务结束----------");
    }

}
